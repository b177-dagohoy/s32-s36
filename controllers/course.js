const Course = require("../models/Course");
const auth = require("../auth");

// Controller function for creating a course
module.exports.addCourse = (courseData) => {
    // Create a variable "newCourse" and instantiates a new course object
    if(courseData.isAdmin){
        let newCourse = new Course({
            name : courseData.course.name,
            description : courseData.course.description,
            price : courseData.course.price
        })

        return newCourse.save().then((course, error) =>{
            if(error){
                return false;
            }
            else{
                return true;
            }
        })
    }
    else{
        return Promise.resolve(false);
    }
}

// Controller function for getting all courses
module.exports.getAllCourses = () => {
    return Course.find({}).then(result => {
        return result
    })
}

// Controller function for getting a specific course
module.exports.getCourse = (reqParams) => {
    return Course.findById(reqParams.courseId).then(result => {
        return result
    })
}

// Controller function for updating a course
module.exports.updateCourse = (reqParams, reqBody) => {
    // Specify the fields/properties of the document to be updated
    let updatedCourse = {
        name : reqBody.name,
        description : reqBody.description,
        price : reqBody.price
    }

    return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((course, error) => {
        if(error){
            return false
        }
        else{
            return true
        }
    })
}

module.exports.archiveCourse = (reqParams) => {

	let updateActiveField = {
		isActive : false
	};

	return Course.findByIdAndUpdate(reqParams.courseId, updateActiveField).then((course, error) => {

		// Course not archived
		if (error) {

			return false;

		// Course archived successfully
		} else {

			return true;

		}

	});
};
