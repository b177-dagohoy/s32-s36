const express = require("express");
const router = express.Router();
const courseController = require("../controllers/course");
const auth = require("../auth");

// Route for creating a course
router.post("/", auth.verify, (req, res) => {
    const courseData = {
        course : req.body,
        isAdmin : auth.decode(req.headers.authorization).isAdmin
    }

    courseController.addCourse(courseData).then(resultFromController => res.send(resultFromController))
})

// Retrieving all courses
router.get("/all", (req, res) => {
    courseController.getAllCourses().then(resultFromController => res.send(resultFromController))
})

// Route for retrieving specific course
router.get("/:courseId", (req, res) => {
    courseController.getCourse(req.params).then(resultFromController => res.send(resultFromController))
})

// Route for updating a course
router.put("/:courseId", auth.verify, (req, res) =>{
    courseController.updateCourse(req.params, req.body).then(resultFromController => res.send(resultFromController))
})

// Route for archiving a course

router.put("/:courseId/archive", auth.verify, (req, res) =>{
    courseController.archiveCourse(req.params).then(resultFromController => res.send(resultFromController))
})

module.exports = router;
